﻿using CheatMenu.MenuGUI;
using UnityEngine;

namespace CheatMenu
{
    public class ModManager
    {
        public static GameObject PluginObject { get; protected set; }

        public ModManager()
        {
            PluginObject = new GameObject();
            SettingsManager.LoadSettings();
            AddMono();
        }

        public void AddMono()
        {
            AddMono<GUICheatMenu>();
            AddMono<GUIResourceMenu>();
            AddMono<GUISettlerTraitsMenu>();
        }

        private void AddMono<T>() where T : Component
        {
            PluginObject.AddComponent(typeof(T));
        }
    }
}
