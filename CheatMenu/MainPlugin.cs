﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Timber_and_Stone.API;
using Timber_and_Stone.API.Event;
using Timber_and_Stone.Event;

namespace CheatMenu
{
    public class MainPlugin : CSharpPlugin, IEventListener
    {
        public override string ModDesc()
        {
            return "A cheat menu. Originally from BobisBackCombinedMods.";
        }

        public override string ModName()
        {
            return "Cheat Menu";
        }

        public override Version ModVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version;
        }

        public override void OnEnable()
        {
            EventManager.getInstance().Register(this);
        }

        public override void OnLoad()
        {
            new ModManager();
        }

        ~MainPlugin()
        {
            SettingsManager.SaveSettings();
        }
    }
}
