﻿using CheatMenu.MenuGUI;
using MithrilAPI.Debug;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CheatMenu
{
    /// <summary>
    /// This enum allows indexing into the bool array called boolSettings. 
    /// This enum is used to help keep track of all the settings in the mod.
    /// </summary>
    public enum Preferences
    {
        //Settler Traits menu
        ApplyTraitsToNewSettlers, ToggleSettlerTraitsMenu,
        //Resource Menu
        ToggleResourceMenu, UnlimitedResources,
        //GUIWindowCheatMenu options
        ToggleCheatMenu, NoHunger, NoInvasions, Invincible, InvasionsInfo, DisableLOS, EternalLight, EternalNight
    };

    /// <summary>
    /// This class handles everything related to settings in the project. This includes but is not limited to loading and saving settings, 
    /// keeping track of hot keys andcontrol groups, as well as any bool preferences in the mod.
    /// </summary>
    class SettingsManager
    {
        /// <summary>
        /// This is the array yhat holds the boolean settings in the mod
        /// </summary>
        public static bool[] BoolSettings = {
            false, false, //init GUIWindowSettlerTraitsMenu
            false, false, //init GUIWindowResourceMenu
            false, false, false, false, false, false, false, false, //init GUIWindowCheatMenu options
        };

        /// <summary>
        /// This keeps track of the hotkeys in the app. There is a key for the hotkey 
        /// combined with a Keycode which is the value for the key.
        /// </summary>
        public static Dictionary<string, KeyCode> HotKeys = new Dictionary<string, KeyCode>() {
            {"toggleCheatMenuHotKey", KeyCode.M}
        };

        /// <summary>
        /// This will load all the settings for the mod form a hard coded settings file name.
        /// </summary>
        public static void LoadSettings()
        {
            String buffer;

            try
            {
                StreamReader sr = new StreamReader("./plugins/configs/CheatMenuConfig.txt");
                buffer = sr.ReadToEnd();

                //get the bool settings arry from teh file
                string[] preferenceNames = Enum.GetNames(typeof(Preferences));
                for (int i = 0; i < BoolSettings.Length && i < preferenceNames.Length; i++)
                {
                    ExtractBoolean(preferenceNames[i], buffer, ref BoolSettings[i]);
                }

                for (int i = 0; i < GUISettlerTraitsMenu.traitsDictionary.Keys.Count; i++)
                {
                    string key = GUISettlerTraitsMenu.traitsDictionary.Keys.ElementAt(i);
                    if (buffer.Contains(key))
                    {
                        GUISettlerTraitsMenu.traitsDictionary[key] = true;
                    }
                }

                //get the hotkeys fro mteh file
                for (int i = 0; i < HotKeys.Count; i++)
                {
                    var hotKey = HotKeys.ElementAt(i);
                    if (buffer.Contains(hotKey.Key))
                    {
                        ExtractHotKey(hotKey, buffer);
                    }
                }

                sr.Close();
                GUIManager.getInstance().AddTextLine("Settings Loaded");
            }
            catch (Exception e)
            {
                WriteExceptionToLog("Settings Failed To Load: ", e);
                GUIManager.getInstance().AddTextLine("Settings Failed To Load");
            }
        }

        /// <summary>
        /// This function will save all the settings for the mod into a hard coded filename
        /// </summary>
        public static void SaveSettings()
        {
            try
            {
                if (!Directory.Exists("./plugins/configs")) Directory.CreateDirectory("./plugins/configs");

                StreamWriter sw = new StreamWriter("./plugins/configs/CheatMenuConfig.txt");

                //write the bool settings file
                sw.WriteLine("//Cheat Menu Settings File Version 1.0");
                sw.WriteLine("//Order does not matter. Variable names must match, must have a space after the var name.");
                sw.WriteLine("//Boolean's must be set to 'True' or 'False' otherwise they will be ignored.");

                //write the hotkeys to the file
                sw.WriteLine("//Below are all the hot keys. All keys must be assigned a string from UnityEngine.KeyCode (Google It)");
                foreach (var hotKey in HotKeys)
                {
                    sw.WriteLine(hotKey.Key + " " + hotKey.Value.ToString());
                }
            }
            catch (Exception e)
            {
                WriteExceptionToLog("Settings Failed To Save Exception: ", e);
            }
        }

        //this will get a bool out of the buffer
        private static void ExtractBoolean(string stringToSearchFor, string buffer, ref bool boolToChange)
        {
            var index = buffer.IndexOf(stringToSearchFor, StringComparison.Ordinal);

            if (index == -1) return;

            var temp = buffer.Substring(index);
            temp = temp.Split()[1];

            if (temp == default(string)) return;

            temp = temp.Trim();
            try
            {
                boolToChange = Convert.ToBoolean(temp);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
                GUIManager.getInstance().AddTextLine("Failed to Convert '" + temp + "' to boolean. make sure it is 'True' or 'False'");
            }
        }

        //this function will extract a hot key out of the buffer
        private static void ExtractHotKey(KeyValuePair<string, KeyCode> hotKey, string buffer)
        {
            var index = buffer.IndexOf(hotKey.Key, StringComparison.Ordinal);

            if (index == -1) return;

            var temp = buffer.Substring(index);
            string keyString = temp.Split()[1];
            try
            {
                HotKeys[hotKey.Key] = (KeyCode)Enum.Parse(typeof(KeyCode), keyString);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
                GUIManager.getInstance().AddTextLine("There was a error in loading the settings for hotkey name '" + hotKey.Key + "' reverting to default hotkey.");
            }
        }

        public static void WriteExceptionToLog(string message, Exception ex)
        {
            File.WriteAllText("./saves/BobisbackLog.txt", "\n\n" + message + " " + ex.Message);
            DebugConsole.Log("Exception: " + ex.Message);
        }
    }
}
